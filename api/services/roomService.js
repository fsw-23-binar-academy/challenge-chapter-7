const roomRepository = require("../repositories/roomRepository");
const { comparing, hashing } = require("../utils/bcrypt");
const { generateToken } = require("../utils/jwt");

const generateCode = () => {
  const character =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let result = "";
  for (let i = 0; i < 6; i++) {
    result += character.charAt(Math.floor(Math.random() * character.length));
  }
  return result;
};

const createRoom = async (req) => {
  try {
    const payload = {
      ownerId: req.user.id,
      code: generateCode(),
      createdAt: new Date(),
      updatedAt: new Date(),
    };

    const paramsFindYourExistingRoom = {
      where: {
        ownerId: req.user.id,
        deletedAt: null,
      },
    };

    const [err, haveRoom] = await roomRepository.findOneRoom(
      paramsFindYourExistingRoom
    );

    if (!err && haveRoom) {
      return [
        {
          message: "You already have a room",
        },
        haveRoom,
      ];
    } else if (!err && !haveRoom) {
      return await roomRepository.createRoom(payload);
    } else {
      return [err, null];
    }
  } catch (err) {
    console.log("error at service room");
    return [err, null];
  }
};

const joinRoom = async (req) => {
  const { code } = req.params;

  try {
    const paramsFindRoom = {
      code,
    };
    const [errFind, room] = await roomRepository.findOneRoom(paramsFindRoom);

    if (!errFind && !room) {
      return [
        {
          message: "Room not exist",
        },
        null,
      ];
    }

    if (!errFind && room) {
      if (room?.challengerId) {
        return [
          {
            message: "Room is full",
          },
          room,
        ];
      }

      if (room?.ownerId === req.user.id) {
        return [
          {
            message: "You are owner of this room",
          },
          room,
        ];
      }

      room.challengerId = req.user.id;
      room.updatedAt = new Date();
      room.save();

      return [null, room];
    }

    if (errFind) {
      return [errFind, null];
    }
  } catch (error) {
    console.log("error at service");
    return [error, null];
  }
};

module.exports = { createRoom, joinRoom };
