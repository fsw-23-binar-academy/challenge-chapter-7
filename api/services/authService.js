const userRepository = require("../repositories/userRepository");
const { comparing, hashing } = require("../utils/bcrypt");
const { generateToken } = require("../utils/jwt");

const doLogin = async (req) => {
  const { username, password } = req.body;
  try {
    const options = {
      where: {
        username,
      },
    };
    const [err, user] = await userRepository.findOneUserRepository(options);
    if (!err && user) {
      const isMatch = await comparing(password, user.password);
      if (isMatch) {
        const token = await generateToken({
          id: user.id,
          username: user.username,
          email: user.email,
          role: user.role,
        });
        if (!token) {
          return [token, null];
        }
        return [null, { token, user }];
      } else {
        return [new Error("Password is incorrect"), null];
      }
    }
  } catch (err) {
    console.log("error at service");
    return [err, null];
  }
};

const register = async (req) => {
  const { username, email, password } = req.body;
  const passwordHash = await hashing(password);

  try {
    const payload = {
      username,
      email,
      password: passwordHash,
      createdAt: new Date(),
      updatedAt: new Date(),
    };
    return await userRepository.createUser(payload);
  } catch (error) {
    console.log("error at service");
    return [error, null];
  }
};

module.exports = { doLogin, register };
