var express = require("express");
var router = express.Router();
const { doLogin, register } = require("../controllers/AuthController");

/* Login Users */
router.post("/login", doLogin);

router.post("/register", register);

module.exports = router;
