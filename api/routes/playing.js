var express = require("express");
var router = express.Router();
const { choose } = require("../controllers/playingController");

router.post("/choose/:room/:round", choose);

// router.get("/:room/:round", playing);

module.exports = router;
