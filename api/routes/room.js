var express = require("express");
var router = express.Router();
const { createRoom, joinRoom } = require("../controllers/roomController");

router.post("/create", createRoom);

router.get("/join/:code", joinRoom);

module.exports = router;
