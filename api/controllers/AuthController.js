const authService = require("../services/authService");
const { handleError, handleSuccess } = require("../utils/response");

const doLogin = async (req, res) => {
  const [err, data] = await authService.doLogin(req);
  console.log("data", data);
  if (err) {
    return handleError(err, res);
  } else {
    return handleSuccess(data, res, {
      message: "Success Login",
      code: 200,
    });
  }
};

const register = async (req, res) => {
  const [err, user] = await authService.register(req);
  if (err) {
    return handleError(err, res);
  } else {
    return handleSuccess(user, res, {
      message: "Success Create User",
      code: 200,
    });
  }
};

module.exports = {
  doLogin,
  register,
};
