const roomService = require("../services/roomService");
const { handleError, handleSuccess } = require("../utils/response");

const createRoom = async (req, res) => {
  //   res.send("create room");
  const [err, data] = await roomService.createRoom(req);
  console.log("data", data);
  if (err) {
    return handleError(err, res);
  } else {
    return handleSuccess(data, res, {
      message: "Success Login",
      code: 200,
    });
  }
};

const joinRoom = async (req, res) => {
  const [err, room] = await roomService.joinRoom(req);
  if (err) {
    return handleError(err, res);
  } else {
    return handleSuccess(room, res, {
      message: "Success Join Room",
      code: 200,
    });
  }
};

module.exports = {
  createRoom,
  joinRoom,
};
