const playingService = require("../services/playingService");
const { handleError, handleSuccess } = require("../utils/response");

const choose = async (req, res) => {
  const [err, data] = await playingService.choose(req);
  //   console.log("data", data);
  if (err) {
    return handleError(err, res);
  } else {
    return handleSuccess(data, res, {
      message: "Success Login",
      code: 200,
    });
  }
};

// const joinRoom = async (req, res) => {
//   const [err, room] = await roomService.joinRoom(req);
//   if (err) {
//     return handleError(err, res);
//   } else {
//     return handleSuccess(room, res, {
//       message: "Success Join Room",
//       code: 200,
//     });
//   }
// };

module.exports = {
  choose,
};
