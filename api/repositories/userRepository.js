const { User } = require("../models");

const findOneUserRepository = async (options) => {
  try {
    const user = await User.findOne(options);
    return [null, user];
  } catch (err) {
    return [err, null];
  }
};

const createUser = async (payload) => {
  try {
    const user = await User.create(payload);
    return [null, user];
  } catch (err) {
    return [err, null];
  }
};

module.exports = { findOneUserRepository, createUser };
