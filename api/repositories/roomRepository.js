const { PlayerRooms } = require("../models");

const findOneRoom = async (options) => {
  try {
    const room = await PlayerRooms.findOne(options);
    return [null, room];
  } catch (err) {
    return [err, null];
  }
};

const createRoom = async (payload) => {
  try {
    const room = await PlayerRooms.create(payload);
    return [null, room];
  } catch (err) {
    return [err, null];
  }
};

const editRoomById = async (roomId, payload) => {
  try {
    const room = await PlayerRooms.update(payload, {
      where: {
        id: roomId,
      },
    });
    return [null, room];
  } catch (err) {
    return [err, null];
  }
};

module.exports = { findOneRoom, createRoom, editRoomById };
