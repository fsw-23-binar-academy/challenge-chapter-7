"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class JankenItems extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  JankenItems.init(
    {
      name: DataTypes.STRING,
      rank: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "JankenItems",
    }
  );
  return JankenItems;
};
