"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class PlayerRooms extends Model {
    static associate(models) {
      // define association here
    }
  }
  PlayerRooms.init(
    {
      code: DataTypes.STRING,
      ownerId: DataTypes.INTEGER,
      challengerId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "PlayerRooms",
    }
  );
  return PlayerRooms;
};
