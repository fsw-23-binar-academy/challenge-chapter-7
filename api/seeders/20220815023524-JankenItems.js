"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */

    await queryInterface.bulkInsert("JankenItems", [
      {
        name: "batu",
        rank: 0,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "kertas",
        rank: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "gunting",
        rank: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
