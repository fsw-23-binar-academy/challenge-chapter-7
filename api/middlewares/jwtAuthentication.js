const { verifyToken } = require("../utils/jwt");

module.exports = (req, res, next) => {
  if (req.headers.authorization) {
    const token = req.headers.authorization.split(" ")[1];

    const isTokenValid = verifyToken(token);

    if (!isTokenValid) {
      return res.status(401).json({
        message: "Token Invalid",
      });
    }
    req.user = {
      id: isTokenValid.id,
    };
    return next();
  }
  return res.status(401).json({
    message: "Token Invalid",
  });
};
