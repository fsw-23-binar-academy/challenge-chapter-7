const handleError = (err, res) => {
  if (err) {
    console.log(err);
    return res.status(500).json({
      message: err.message,
    });
  }
};

const handleSuccess = (data, res, message) => {
  console.log("data", data);
  return res.status(message.code).json({
    data,
    message: message.message,
  });
};

module.exports = { handleError, handleSuccess };
