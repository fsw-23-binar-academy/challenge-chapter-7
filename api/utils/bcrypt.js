const bcrypt = require("bcrypt");

module.exports = {
  comparing: async (data, dataHash) => {
    return await bcrypt.compare(data, dataHash);
  },
  hashing: async (params) => {
    return await bcrypt.hash(params, 10);
  },
};
