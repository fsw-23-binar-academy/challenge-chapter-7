import Link from "next/link";
import { useEffect, useState } from "react";

function Navbar() {
  const [bgNavbar, setBgNavbar] = useState("bg-transparent");
  useEffect(() => {
    document.addEventListener("scroll", () => {
      console.log(window.scrollY);
      if (window.scrollY > 80) {
        setBgNavbar("bg-black/50 backdrop-blur-sm");
      } else {
        setBgNavbar("bg-transparent");
      }
    });
  }, []);

  return (
    <div className={`${bgNavbar} h-20 w-full flex px-20 sticky top-0`}>
      <div className=" w-64 flex  items-center">
        <div className="text-white">LOGO</div>
      </div>
      <div className=" flex-1 flex space-x-5 items-center">
        <div className="text-white">HOME</div>
        <div className="text-white">WORK</div>
        <div className="text-white">CONTACT</div>
        <div className="text-white">ABOUT ME</div>
      </div>
      <div className=" w-56 flex items-center space-x-5 justify-end">
        <div className="text-white">
          <Link href="/register">SIGN UP</Link>
        </div>
        <div className="text-white">
          <Link href="/login">LOGIN</Link>
        </div>
      </div>
    </div>
  );
}

export default Navbar;
