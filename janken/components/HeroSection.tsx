import Image from "next/image";
import React from "react";

function HeroSection() {
  const containerVariants = {
    hidden: {
      opacity: 0,
      x: "100vw",
    },
    visible: {
      opacity: 1,
      x: 0,
      transition: { type: "spring", delay: 0.5 },
    },
    exit: {
      x: "-100vh",
      transition: { ease: "easeInOut" },
    },
  };

  const nextVariants = {
    hidden: {
      x: "-100vw",
    },
    visible: {
      x: 0,
      transition: { type: "spring", stiffness: 120 },
    },
  };

  const buttonVariants = {
    hover: {
      scale: 1.1,
      textShadow: "0px 0px 8px rgb(255,255,255)",
      boxShadow: "0px 0px 8px rgb(255,255,255)",
      transition: {
        duration: 0.3,
        yoyo: Infinity,
      },
    },
  };

  return (
    <div className="min-h-screen bg-hero-landing bg-cover bg-center w-full -mt-20 pt-20 flex">
      <div className="flex-1 flex flex-col items-center">
        <div className="flex-1 flex items-center">
          <div className="flex-1 flex-col space-y-5">
            <div className="text-white text-center text-7xl font-bold">
              PLAY TRADITIONAL GAMES
            </div>
            <div className="text-white text-center text-2xl">
              Experience new traditional play game
            </div>
            <div className="text-center">
              <button className="bg-[#FFB548] py-4 px-20 font-bold ">
                PLAY NOW
              </button>
            </div>
          </div>
        </div>

        <div className="h-20 flex-col items-center space-y-2 bg-black/30 px-4 py-2 rounded-lg animate-bounce">
          <div className="text-white text-center">The Story</div>
          <div className="flex justify-center">
            <Image
              src="/icons/scroll-down.svg"
              alt="scroll icon"
              className=""
              width={24}
              height={12}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default HeroSection;
