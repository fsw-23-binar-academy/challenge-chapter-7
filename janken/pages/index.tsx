import type { NextPage } from "next";
import Image from "next/image";
import HeroSection from "../components/HeroSection";
import Navbar from "../components/Navbar";
import styles from "../styles/Home.module.css";

const LandingPage: NextPage = () => {
  return (
    <>
      <Navbar />
      <HeroSection />
      <div className="min-h-screen bg-[#FFB548]">
        <div className="flex-1 min-h-screen bg-black/60"></div>
      </div>
    </>
  );
};

export default LandingPage;
